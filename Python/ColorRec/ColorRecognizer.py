from PIL import Image
from log.logger import Logger

class ColorRec (object):
    logger:Logger
    def __init__(self):
        self.logger = Logger()

    def openImag(self, file_path):
        im = Image.open(file_path)  # Can be many different formats.
        return im

    def readPixel(self,im, x, y):
        pix = im.load()
        print(im.size)  # Get the width and hight of the image for iterating over
        rgb = pix[x, y] # Get the RGBA Value of the a pixel of an image
        print(rgb)
        return rgb

    def read_img_size(self, file_path):
        im = Image.open(file_path)
        return im.size
