from Python.ColorRec.ColorRecognizer import ColorRec


def read_standard_color():
    standard_img = "../../doc/standard_color.png"
    colorRec = ColorRec()
    img = colorRec.openImag(standard_img)
    standard_img_size = img.size
    print(standard_img_size)

if __name__ == "__main__":
    imgfile = "../../doc/color_src.png"
    colorRec = ColorRec()
    img = colorRec.openImag(imgfile)
    rgb_value = colorRec.readPixel(img, 100, 200)

    print(str(rgb_value))

    read_standard_color()


