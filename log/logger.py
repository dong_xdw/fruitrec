import logging, os

class Logger:
    LOG_FILE = 'log.txt'
    LOG_FORMAT = "%(message)s"

    def __init__(self, clean=False):
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.DEBUG)
        formatter = logging.Formatter(self.LOG_FORMAT)

        if clean:
            if os.path.isfile(self.LOG_FILE):
                with open(self.LOG_FILE, 'w') as f:
                    pass

        fh = logging.FileHandler(self.LOG_FILE)
        fh.setLevel(logging.DEBUG)
        fh.setFormatter(formatter)

        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        ch.setFormatter(formatter)

        self.logger.addHandler(ch)
        self.logger.addHandler(fh)

    def debug(self, *args):
        s = ''
        for i in args:
            s += (str(i) + ' ')

        logging.debug(s)



